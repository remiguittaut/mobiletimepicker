# Mobile Time Picker
This project is an Html5 / javascript implementation of Google material design TimePicker.
## Installation
After having cloned the project on a directory, open a command prompt there.

Git and nodejs / npm must be installed and reachable from the path.

First, let's restore node packages:
```
npm install
```
Then, let's restore bower packages (client-side scripts):
```
bower install
```
All the packages should now be installed.
There is a bug in some versions of the bower package 'angular-linq'. Open the file bower_components/angular-linq/angular-linq.js and on the top (line 8), change:
```
angular.module('angular-linq')
```
to:
```
angular.module('angular-linq', [])
```
(If the line is already like this, just ignore this step).

Finally, we have to build scripts and styles with gulp. In command prompt:
```
gulp
```
## Usage
Once everything above is done, start the server to host the test page:
```
node server/server.js
```
The app is available on http://localhost:8080. (the port can be changed in server/server.js if needed).
## Credits
Remi Guittaut.