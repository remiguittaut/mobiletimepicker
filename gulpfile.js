var gulp = require('gulp');
var del = require('del');
var es = require('event-stream');
var runSequence = require('run-sequence');

var plugins = require("gulp-load-plugins")({
	pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
	replaceString: /\bgulp[\-.]/
});

var dest = 'www/';

gulp.task('js', function() {
	
	plugins.util.log('starting js task');
	
	var jsFiles = [].concat(
		plugins.mainBowerFiles(), [
			'src/app/timePickerApp.js', 
			'src/app/!(timePickerApp)*.js'
		]
	);
	
	plugins.util.log('*** js files');
	
	for(var i = 0; i < jsFiles.length; i++) {
		plugins.util.log(jsFiles[i]);	
	}
	
	var src = gulp.src(jsFiles); 
	
	var dev = src.pipe(plugins.filter('*.js'))
		.pipe(plugins.concat('app.js'));
	
	var release = src.pipe(plugins.filter('*.js'))
		.pipe(plugins.concat('app.min.js'))
		.pipe(plugins.uglify());
		
	var task = es.merge(dev, release)
		.pipe(gulp.dest(dest));
		
	plugins.util.log('returning js task');
		
	return task;
});

gulp.task('html', function() {
	
	plugins.util.log('starting html task');
	
	var task = gulp.src('src/*.html')
		.pipe(gulp.dest(dest)); 
	
	plugins.util.log('returning html task');
		
	return task;
});

gulp.task('assets', function() {
	
	plugins.util.log('starting assets task');
	
	var task = gulp.src('src/assets/**.*')
		.pipe(gulp.dest(dest)); 
	
	plugins.util.log('returning assets task');
		
	return task;
});

gulp.task('styles', function() {
	
	plugins.util.log('starting styles task');
	
	var task = gulp.src([
			'bower_components/angular-toggle-switch/angular-toggle-switch.css',
			'src/styles/*.css',
			'src/styles/*.less'
		])
		.pipe(plugins.concat('all.min.css'))
		.pipe(plugins.less())
		.pipe(plugins.cssmin())
		.pipe(gulp.dest(dest)); 
	
	plugins.util.log('returning styles task');

	return task;
});

gulp.task('cleanup', function() {
	plugins.util.log('starting cleanup task');
	
	var task = del([dest + '**/*.*']);
	
	plugins.util.log('returning cleanup task');
	
	return task;
});

gulp.task('default', function(callback) {
	runSequence(
		'cleanup',
		'html',
		'assets',
		'js',
		'styles',
		function (error) {
			if (error) {
				console.log(error.message);
			} else {
				console.log('BUILD FINISHED SUCCESSFULLY');
			}
			callback(error);
		});
});

gulp.task('watch', function(callback) {
	gulp.watch('src/*.html', ['html']);
	gulp.watch(['src/app/*.js'], ['js']);
	gulp.watch('src/assets/**.*', ['assets']);
	gulp.watch('src/styles/**.*', ['styles']);
});
