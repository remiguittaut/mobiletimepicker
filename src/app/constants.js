angular.module('timePickerApp')
	.value('constants', {
		
		PickerModes: {
			Hours: 1,
			Minutes: 2
		},
		
		TimeModes: {
			AM: 'am',
			PM: 'pm',
			H24: 'h24'
		},
		
		TimeStyles: {
			AmPm: 'amPm',
			H24: 'h24'
		},
		
		Themes: {
			Light: 'light',
			Dark: 'dark'
		}
	});
	