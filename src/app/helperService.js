angular.module('timePickerApp')
	.service('helper', ['$linq', '$window', function($linq, $window) {
		
		var s = this;
		
		var e = $linq.Enumerable();
		
		s.calculateAngles = calculateAngles;
		s.findClosestIndex = findClosestIndex;
		s.findAngleFromPoint = findAngleFromPoint; 
		s.padNumber = padNumber;
		s.isPointInsideCircle = isPointInsideCircle;
		s.getDigits = getDigits;
		s.isiPhone = isiPhone;
		s.isPortrait = isPortrait;
		
		function isiPhone() {
			return navigator.userAgent.match(/(iPhone)/) !== null 
				&& navigator.userAgent.match(/AppleWebKit/) !== null;
		}
		
		function isPortrait() {
			return window.matchMedia("(orientation: portrait)").matches;
		}
		
		function getDigits() {
			return {
				Hours_AM_PM: e.RangeDown(12, 12).ToArray(),
				Hours_H24: e.RangeDown(24, 24).ToArray(),
				Minutes: e.From(0).Concat(e.RangeDown(55, 11, 5).ToArray()).ToArray()
			};
		}
		
		function isPointInsideCircle(point, origin, radius) {
			return Math.pow(point.x - origin.x, 2) + Math.pow(point.y - origin.y, 2)
				<= Math.pow(radius, 2);
		}
		
		function padNumber(nb, length) {
			var str = '' + nb;
			while (str.length < length) {
				str = '0' + str;	
			}
			return str;
		}
		
		function calculateAngles(digits) {
			var digitsData = $linq.Enumerable().From(digits);
			
			digitsData = digitsData.Select(function (d, i) {
				var angle = (((Math.PI * 2) / digitsData.Count()) * i + (Math.PI / 2)) % (2 * Math.PI); 
				return {
					data: d,
					angle: angle
				};
			}).ToArray();
			
			return digitsData;
		}
		
		function findAngleFromPoint(point) { 
			return (Math.atan2(-1 * point.y, point.x) + (2 * Math.PI)) % (2 * Math.PI); 
		}
		
		function findClosestLine(point, digits, radius) {
			var digitsData = calculateAngles(digits);
			
			var linesToDigits = $linq.Enumerable().From(digitsData)
				.Select(function(d, i) {
					return {
						index: i,
						angle: d.angle,
						x1: 0,
						y1: 0,
						x2: Math.round(Math.cos(d.angle) * radius),
						y2: -1 * Math.round(Math.sin(d.angle) * radius)
					}
				});
				
			return linesToDigits.MinBy(function(l) {
				return distToSegment(point, l); 
			});
		}
		
		function findClosestIndex(point, digits, radius) {
			return findClosestLine(point, digits, radius).index; 
		}
	
		/* Algorithm to calculate the distance from a point to a segment, shamelessly copied from 
			http://stackoverflow.com/a/1501725 */
		
		function sqr(x) { return x * x }
		
		function dist2(segment) { return sqr(segment.x1 - segment.x2) + sqr(segment.y1 - segment.y2) }

		function distToSegmentSquared(p, segment) {
			var l2 = dist2(segment);
			
			if (l2 == 0) {
				return dist2({
					x1: p.x,
					x2: segment.x1,
					y1: p.y,
					y2: segment.y1
				});
			}
			
			var t = ((p.x - segment.x1) * (segment.x2 - segment.x1) + (p.y - segment.y1) * (segment.y2 - segment.y1)) / l2;
			
			if (t < 0) {
				return dist2({
					x1: p.x,
					x2: segment.x1,
					y1: p.y,
					y2: segment.y1
				});
			}
			
			if (t > 1) {
				return dist2({
					x1: p.x,
					x2: segment.x2,
					y1: p.y,
					y2: segment.y2
				});
			}
			
			return dist2({
				x1: p.x,
				x2: (segment.x1 + t * (segment.x2 - segment.x1)),
				y1: p.y,
				y2: segment.y1 + t * (segment.y2 - segment.y1)
			});
		}

		function distToSegment(p, segment) { return Math.sqrt(distToSegmentSquared(p, segment)); }
	}])