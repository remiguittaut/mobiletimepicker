angular.module('timePickerApp', ['angular-linq', 'ngTouch', function(){ }])

	.filter('pad2', ['helper', function(helper) {
		return function(input, all) {
			return helper.padNumber(input, 2);
		}
	}])
	
	.directive('onTouchstart', function() {
		
		function link(scope, element, attrs) {
			var ontouchFn = scope.$eval(attrs.onTouchstart);
            element.on('touchstart', function(evt) {
                scope.$apply(function() {
                    ontouchFn.call(scope, evt);
                });
            });
		}
		
		return {
			restrict: 'A',
			link: link
		}
	})
	
	.directive('onTouchmove', function() {
		
		function link(scope, element, attrs) {
			var ontouchFn = scope.$eval(attrs.onTouchmove);
            element.on('touchmove', function(evt) {
                scope.$apply(function() {
                    ontouchFn.call(scope, evt);
                });
            });
		}
		
		return {
			restrict: 'A',
			link: link
		}
	})
		
;