angular.module('timePickerApp')
	.directive('timePickerClockHand', ['$linq', 'helper', function($linq, helper) {
		var controller = ['$scope', function($scope) {
			$scope.render = render;
			
			$scope.$watch('digitsData', renderIfChanged);
			$scope.$watch('radius', renderIfChanged);
			$scope.$watch('angle', renderIfChanged);
			$scope.$watch('quadrant', renderIfChanged);
		
			$scope.$watch('forceAnimate', function(value) {
				if(value) {
					var digitsData = helper.calculateAngles($scope.digitsData)
					var angle = digitsData[$scope.selectingIndex].angle;
					$scope.forceAnimate = false;
					render(angle);	
				}
			});
		
			$scope.$watch('selectingIndex', function(newV, oldV) {
				if(newV !== oldV || $scope.selectedIndex !== $scope.selectingIndex) {
					var digitsData = helper.calculateAngles($scope.digitsData)
					var angle = digitsData[$scope.selectingIndex].angle;
					render(angle);
				}
			});
		
			function renderIfChanged(newV, oldV) {
				if(newV !== oldV) {
					render();
				}
			};
			
			function render(animateToAngle) {
				var group = $scope.quadrant.select('g.clock-hand');
				
				if(angular.isNumber(animateToAngle)) {
					group.transition()
						.ease('cubic-in-out')
						.duration(200)
						.tween('rotationAnim', function() {
							var previous = $scope.angle;
							
							if(Math.abs(animateToAngle - previous) > Math.PI) {
								if(previous < animateToAngle) {
									previous += 2 * Math.PI;
								} else {
									animateToAngle += 2 * Math.PI;
								}
							}
							
							var interpolate = d3.interpolateNumber(previous, animateToAngle);
							
							return function(t) {
								var current = interpolate(t);
								var x = Math.round(Math.cos(current) * $scope.radius);
								var y = -1 * Math.round(Math.sin(current) * $scope.radius);
								
								d3.select(this).select('line.hand-line')
									.attr('x2', x)
									.attr('y2', y);
								
								d3.select(this).select('circle.hand')
									.attr('cx', x)
									.attr('cy', y);
							};
						})
						.each('end', function() {
							$scope.$apply(function() {
								$scope.angle = animateToAngle % (2 * Math.PI);
								$scope.selectedIndex = $scope.selectingIndex;
							});
						});
				} else {
					var x = Math.round(Math.cos($scope.angle) * $scope.radius);
					var y = -1 * Math.round(Math.sin($scope.angle) * $scope.radius);
					
					group.select('line.hand-line')
						.attr('x2', x)
						.attr('y2', y);
					
					group.select('circle.hand')
						.attr('cx', x)
						.attr('cy', y);
				}
			};
		}];
		
		var link = function(scope, element) {
			scope.quadrant = d3.select(element.parent()[0]);
			
			var group = scope.quadrant.append('g')
				.classed('clock-hand', true);
			
			group.append('line')
				.classed('hand-line', true)
				.attr('x1', 0)
				.attr('y1', 0);	
			
			group.append('circle')
				.classed('hand', true)
				.attr('r', 60);
			
			scope.render();
		}
		
		return {
			restrict: 'E',
			replace: true,
			require: '^timePickerPanel',
			link: link,
			controller: controller,
			scope: {
				'digitsData': '=digitsData',
				'selectedIndex': '=selectedIndex',
				'selectingIndex': '=selectingIndex',
				'radius': '=radius',
				'angle': '=angle',
				'forceAnimate': '=forceAnimate'
			}
		}
	}]);