angular.module('timePickerApp')
	.controller('timePickerController', ['$scope', 'constants', function($scope, c) {
		
		$scope.TimeStyles = c.TimeStyles;
		$scope.Themes = c.Themes;
		$scope.TimeModes = c.TimeModes;
		
		$scope.picker1Time = {
			h: 3,
			m: 20,
			type: c.TimeModes.PM
		};
		
		$scope.picker1Theme = c.Themes.Light;
		$scope.picker1TimeStyle = c.TimeStyles.AmPm;
		
		$scope.picker2Time = {
			h: 5,
			m: 53,
			type: c.TimeModes.PM
		};
		
		$scope.picker2Theme = c.Themes.Light
		$scope.picker2TimeStyle = c.TimeStyles.AmPm;
		
		$scope.picker3Time = {
			h: 20,
			m: 18,
			type: c.TimeModes.H24
		};
		
		$scope.picker3Theme = c.Themes.Light
		$scope.picker3TimeStyle = c.TimeStyles.H24;
	}])
;