angular.module('timePickerApp')
	.directive('timePickerDigits', ['$linq', 'helper', function($linq, helper) {
		var controller = ['$scope', function($scope) {
			$scope.render = render;
			
			$scope.$watch('digitsData', renderIfChanged);
			$scope.$watch('radius', renderIfChanged);
			$scope.$watch('selectedIndex', renderIfChanged);
			$scope.$watch('quadrant', renderIfChanged);
		
			render();
		
			function renderIfChanged(newV, oldV) {
				if(newV !== oldV) {
					render();
				}
			};
			
			function render() {
				if(!$scope.quadrant || !$scope.digitsData || !$scope.radius)
					return;
				
				var digitsData = helper.calculateAngles($scope.digitsData)
				
				var digits = $scope.quadrant.selectAll('text.digit')
					.data(digitsData);
				
				digits.enter().append('text')
					.attr('alignment-baseline', 'middle')
					.classed('digit',  true);
				
				digits.exit().remove();
				
				digits
					.classed('selected', function(_, i) { return i === $scope.selectedIndex; })
					.attr('x', function (d, i) {
						return Math.round(Math.cos(d.angle) * $scope.radius);
					})
					.attr('y', function (d, i) {
						return -1 * Math.round(Math.sin(d.angle) * $scope.radius);
					})
					.text(function(d) { 
						return $scope.padDigits 
							? helper.padNumber(d.data, 2) 
							: ('' + d.data); 
					});	
			}
		}];		
		
		var link = function(scope, element) {
			scope.quadrant = d3.select(element.parent()[0]);
			element.remove();
			scope.render();
		}
		
		return {
			require: '^timePickerPanel',
			restrict: 'E',
			link: link,
			controller: controller,
			scope: {
				'digitsData': '=digitsData',
				'radius': '=radius',
				'selectedIndex': '=selectedIndex',
				'padDigits': '=padDigits'
			}
		}
	}]);