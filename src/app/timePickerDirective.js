angular.module('timePickerApp')
	.directive('timePicker', function() {
		
		var controller = ['$scope', '$window', '$timeout', '$linq', 'helper','constants', 
			function($scope, $window, $timeout, $linq, helper, c) {
				$scope.Themes = c.Themes;
				$scope.TimeStyles = c.TimeStyles;
				$scope.isPickerOpen = false;
				$scope.editTime = null;
				
				$scope.isiPhone = helper.isiPhone();
				
				$scope.Digits = helper.getDigits();
				
				var editing = false;
				
				$scope.open = function() {
					editing = true;
					$scope.editTime = angular.copy($scope.time);
					$scope.isPickerOpen = true;
				};
				
				$scope.ok = function() {
					$scope.time = angular.copy($scope.editTime);
					$scope.isPickerOpen = false;
				}
				
				$scope.cancel = function() {
					$scope.isPickerOpen = false;
					$scope.editTime = null;
					editing = false;
				}
				
				$scope.onTouchStart = function(e) {
					// $window.alert('touchStart');
					e.preventDefault();
					e.stopImmediatePropagation();
				}
				
				$scope.onTouchMove = function(e) {
					$window.alert('touchMove');
					e.preventDefault();
					e.stopImmediatePropagation();
				}
				
				$scope.$watch('timeStyle', function(newV, oldV) {
					if(newV !== oldV) {
						$scope.time = normalizeTime($scope.time)	
					}
				});
				
				$scope.$watch('time', function(newV, oldV) {
					if(!editing) {
						$scope.time = normalizeTime($scope.time)	
					} else {
						editing = false;
					}
				});
				
				function normalizeTime(time) {
					if($scope.timeStyle === c.TimeStyles.H24) {
						if(time.type === c.TimeModes.PM) {
							time.h = time.h + 12;
						}
						time.type = c.TimeModes.H24;
					} else if($scope.timeStyle !== c.TimeStyles.H24) {
						if(time.h > 12) {
							time.h = time.h % 12;
							time.type = c.TimeModes.PM;
						} else {
							time.type = c.TimeModes.AM;
						}
					}
					
					if($scope.timeStyle === c.TimeStyles.AmPm) {
						if(!(time.h >= 1 && time.h <= 12)) {
							time.h = 12;
						}
					} else {
						if(!(time.h >= 1 && time.h <= 24)) {
							time.h = 12;
						}
					}
					
					if(time.m < 0) {
						time.m = 0;
					} else if(time.m > 55) {
						time.m = 55;
					} else if($scope.Digits.Minutes.indexOf(time.m) === -1) {
						time.m = $linq.Enumerable()
							.From($scope.Digits.Minutes)
							.MinBy(function(d) {
								return Math.abs(time.m - d);
							})
					}
					
					return time;
				}
			}];
			
			return {
				restrict: 'E',
				scope: {
					'theme': '=theme',
					'time': '=time',
					'timeStyle': '=timeStyle'
				},
				controller: controller,
				templateUrl: 'timePickerTemplate.html'
			}
			
		})
	
;