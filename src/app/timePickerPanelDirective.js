angular.module('timePickerApp')
	.directive('timePickerPanel', ['$linq', '$window', 'helper', 'constants', function($linq, $window, helper, c) {
		
		var controller = ['$scope', function($scope) {
		
			$scope.clockDimension = null;
			
			$scope.PickerModes = c.PickerModes;
			$scope.TimeModes = c.TimeModes;
			$scope.TimeStyles = c.TimeStyles;
			$scope.Themes = c.Themes;
		
			$scope.Digits = helper.getDigits();
			
			$scope.pickerMode = c.PickerModes.Hours;
			
			$scope.digits = $scope.timeStyle === c.TimeStyles.AmPm 
				? $scope.Digits.Hours_AM_PM
				: $scope.Digits.Hours_H24;
			
			// in the invariant cordinates system of the svg (-500, 500  -500 500), 	
			$scope.radius = 380;
			$scope.angle = 0;
			
			$scope.theme = c.Themes.Light;
		
			$scope.forceAnimate = false;
		
			$scope.$watch('pickerMode', renderIfChanged);
			$scope.$watch('time', renderIfChanged);
			$scope.$watch('timeStyle', renderIfChanged);
			
			$scope.$watch('selectedIndex', function(newV, oldV) {
				if(newV !== oldV && newV !== null) {
					if($scope.pickerMode === c.PickerModes.Hours) {
						if($scope.timeStyle === c.TimeStyles.AmPm) {
							$scope.time.h = $scope.Digits.Hours_AM_PM[$scope.selectedIndex];	
						} else {
							$scope.time.h = $scope.Digits.Hours_H24[$scope.selectedIndex];
						}
					} else {
						$scope.time.m = $scope.Digits.Minutes[$scope.selectedIndex];
					}
				}
			});
			
			render();
			
			function renderIfChanged(newV, oldV) {
				if(newV !== oldV) {
					render();
				}
			}
		
			function render() {
				if($scope.time) {
					var oldDigitsLength = $scope.digits.length;
					var digitsChanged = false; 
					
					$scope.digits = $scope.pickerMode === c.PickerModes.Hours 
						? ($scope.timeStyle === c.TimeStyles.AmPm
							? $scope.Digits.Hours_AM_PM
							: $scope.Digits.Hours_H24)
						: $scope.Digits.Minutes;
					
					digitsChanged = $scope.digits.length !== oldDigitsLength;
					
					$scope.selectingIndex = $scope.pickerMode === c.PickerModes.Hours
						? $scope.digits.indexOf($scope.time.h)
						: $scope.Digits.Minutes.indexOf($scope.time.m);
						
					if(digitsChanged) {
						$scope.selectedIndex = null;
						$scope.forceAnimate = true;
					}	
				}
			}
		}];
		
		var link = function (scope, element) {
			element.on('touchstart touchmove touchend', disableScroll);
			
			scope.$on('$destroy', function() {
				element.off('touchstart', disableScroll);
				element.off('touchmove', disableScroll);
				element.off('touchend', disableScroll);
				$window.removeEventListener('orientationchange', updateDimensions);
			});
			
			function disableScroll(e) {
				e.stopImmediatePropagation();
			}
			
			var quadrant = d3.select(element.find('svg')[0]);
			
			$window.addEventListener('orientationchange', updateDimensions);
			
			updateDimensions();
			
			var point = null;
			
			quadrant.on('touchstart', onTouchStart);
			
			function onTouchStart() {
				var coordinates = d3.touches(this);
				
				if (helper.isPointInsideCircle({
					x: coordinates[0][0],
					y: coordinates[0][1]
				}, { x: 0, y:0 }, scope.radius)) {
					scope.$apply(function() {
						scope.selectedIndex = null;
					});
					
					point = {
						x: coordinates[0][0],
						y: coordinates[0][1]
					};
				}
				
				quadrant.on('touchmove', onTouchMove);
				quadrant.on('touchend', onTouchEnd);
			}
			
			function onTouchMove() {
				var coordinates = d3.touches(this);
				
				if (helper.isPointInsideCircle({
					x: coordinates[0][0],
					y: coordinates[0][1]
				}, { x: 0, y:0 }, scope.radius)) {
					setAngle(coordinates[0]);
				}
				d3.event.preventDefault();
			}
			
			function onTouchEnd() {
				quadrant[0][0].removeEventListener('touchmove', onTouchMove);
				quadrant[0][0].removeEventListener('touchend', onTouchEnd);
				
				if(point) {
					scope.$apply(function() {
						scope.selectingIndex = helper.findClosestIndex(point, scope.digits, scope.radius);
					});	
				}
			}
			
			function setAngle(coords) {
				point = {
					x: coords[0],
					y: coords[1]
				};
				
				scope.$apply(function() {
					scope.angle = helper.findAngleFromPoint(point);
				});
			}
			
			function updateDimensions() {
				var isPortrait = helper.isPortrait();
			
				if(isPortrait) {
					if(($window.innerHeight - 165) < $window.innerWidth) {
						quadrant.attr('width', $window.innerHeight - 165);
						quadrant.attr('height', $window.innerHeight - 165);	
					}	
				} else {
					quadrant.attr('width', $window.innerHeight - 65);
					quadrant.attr('height', $window.innerHeight - 65);
				}
			}
		};
		
		return {
			controller: controller,
			link: link,
			restrict: 'E',
			templateUrl: '/timePickerPanelTemplate.html',
			scope: {
				'time': '=time',
				'theme': '=theme',
				'timeStyle': '=timeStyle',
				'ok': '&ok',
				'cancel': '&cancel',
				'hideButtons': '=hideButtons'
			} 
		}
	}]);